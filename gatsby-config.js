// This is a workaround to https://github.com/gatsbyjs/gatsby/issues/15486
// till https://github.com/gatsbyjs/gatsby/issues/16242 is fixed
const gatsbyRemarkPlugins = [
  {
    resolve: `gatsby-remark-images`,
    options: {
      maxWidth: 1200
    }
  }
]

module.exports = {
  siteMetadata: {
    title: `PCMH Restore Health`,
    description: ` Welcome to Restore Health Center! A patient centered medical home. Our team includes not only family physicians, but also a psychotherapist and a clinical pharmacologist who are dedicated to providing patients with comprehensive services to benefit their health and well-being. With our streamlined registration system and responsive diagnosis procedure, we have more time to focus on helping patients find treatments to feel better, and connecting them with resources to help them stay that way. `,
    author: `@pcmhrestorehealth`,
    email: `pcmhrestorehealth12@gmail.com`,
    phone: `+918971544066`,
    street_address: `#20/7, 2nd Main Road`,
    locality: `VV Giri Colony, Seshadripuram, Bangalore`
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `pages`,
        path: `${__dirname}/src/pages`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `blog`,
        path: `${__dirname}/src/posts`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `data`,
        path: `${__dirname}/src/data`,
      },
    },
    {
      resolve: `gatsby-plugin-page-creator`,
      options: {
        path: `${__dirname}/src/posts`
      }
    },
    `gatsby-transformer-yaml`,
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-mdx`,
      options: {
        extensions: [`.mdx`, `.md`],
        defaultLayouts: {
          'pages': require.resolve('./src/components/layout.js'),
          'blog': require.resolve('./src/components/layout-blog.js')
        },
        gatsbyRemarkPlugins,
        plugins: gatsbyRemarkPlugins
      }
    },
    `gatsby-plugin-netlify`,
    `gatsby-plugin-nprogress`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `PCMH Restore Health`,
        short_name: `Restore Health`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/logo.png`, // This path is relative to the root of the site.
      },
    },
    `gatsby-plugin-offline`,
  ],
}
