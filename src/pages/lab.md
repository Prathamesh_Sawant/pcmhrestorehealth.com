---
title: Lab
---

Molecular Solutions Care Health  works in close association with RH. It was started with the sole purpose of making advanced molecular tests available to anyone in need.

We are a group of molecular biologists, biochemists and microbiologists who love developing novel ways of testing, thereby making them easier, more reliable and inexpensive.

Mission:  To make healthcare, with a special focus on diagnostics, available and accessible.

Vision:  A world where anyone who is sick does not worry about how they are going to manage their treatment and how the treatment will affect their lives and the lives of their loved ones.

hiv testing

1. Viral load testing
2. Drug resistance genotyping
3. Pre-ART and on-ART testing packages
4. Pre-marital and pre-conception packages

viral hepatitis

1. Viral loads for HCV, HBV​

2. Subtype genotyping for HCV

3. Dialysis packages

4. Obstetric packages

screening tests

1. Hep B card test

2. Hep C card test

3. VDRL for syphilis

4. 4th generation HIV testing

5. 3rd generation HIV testing


customized assays

Do you want a test that can be made cheaper, but of the same or higher quality, than what is currently available?

Contact us!


