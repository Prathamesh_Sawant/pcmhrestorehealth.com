---
title: "Home Health Care"
redirect_from:
  - /elderly-care
---

Home Healthcare a growing need in Indian scenario. It is not limited just to elderly population but those who would want our service at their homes or who are unable to access doctors at the clinics, wait in long queues and requires assistance for their daily activities.

The 2017 Indian National Health Policy majorly focused on primary healthcare to provide home healthcare for the need and promote palliative and mental health support especially to the elderly.

## Who needs home-care?

* Patients who are unable to access the clinic (no age limit).
* Expensive transportation.
* Requiring personalised care.

![](../images/who-needs-home-care.jpeg)

## What do we have to offer?

We at Restore Health have created a schematic multi-disciplinary team approach with a step by step assessment of the patient in need to provide optimal therapeutic, psychological support and not limiting to the point of care services, thereby trying to rule out the central cause and reduce the unnecessary pill burden all this in the confines of their comfort zone. We don’t just evaluate the need, but help patients and family promote quality of life beyond those needs. We have a team that work closely with a passion towards the home bound patient to provide life to their years.


![](../images/what-we-offer-home-care.jpeg)

## The services involve

1. Follow-up Post Discharge/Surgeries.
2. Reducing Poly-Pharmacy (on multiple medications).
3. Customised/Individualised Medication Plan.
4. Palliative Support.
5. Psychological Support.
6. Vaccination.
7. Nutritional Care Plan.
8. Providing Nursing Care.
9. Improving quality of life.

## Elderly Care

There are over 120 million people over the age of 60 years in India, and is estimated to grow by 8% every 2 years. Most elderly are homebound and unable to access primary care. Majority of elderly have multiple long standing diseases and are on ploy-pharmacy, sometimes multiple specialists monitor patients for each comorbidity. This reduces optimal therapeutic benefit and lead to reduced adherence and increased drug related events (Adverse drug reaction and Drug drug reaction).

Ageing is a general physiologic process that is yet poorly understood. These days our population is ageing and our life expectancy is increasing. Over the past centuries, improvement in nutrition, public health measures to prevent disease and advance in medicine have protected us from premature death, so that a larger percentage of our population lives decades longer. By enabling our citizens to grow older, we have changed the pattern of disease and death in our society. One way to use ***Technology*** and ***Compassion*** to ***extend senior living*** at ***home*** so that they can be in ***familiar surroundings, hanging out*** with familiar ***people***, ***savouring*** familiar ***memories***. Let’s act before its too late.

## Benefits

* Services to reduce pill-burden.
* Personalised environment comfortable for both the patient and healthcare professional.
* Providing ease of access to healthcare professionals at your doorstep.
* Reduce long waiting hours at the clinic/s.
* Reduce frequency of hospitalisation.

<form action="https://forms.gle/6F4J6gN8KaF8uazM6">
<button type="submit" class="button is-info">Book Appointment Online</button>
</form>
(contact us for more details)
