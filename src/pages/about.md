---
title: "About"
---

At PCMH Restore Health Center, our mission is simple - to make you feel and stay healthy. Whether you’re coming in for a regular checkup or an urgent treatment, our dedicated team of health practitioners have the skills and resources to take care of your needs.

​

Our interdisciplinary team aims not only to elevate the health of our patients but also to inculcate lifestyle modifications targeting long term health benefits.


PCMH Restore Health is a patient centered medical home which puts the patient at the center of the decision-making process and offers discreet, comprehensive, team-based, coordinated, accessible, high quality, and compassionate care. We believe that a holistic approach is required for optimal and sustainable health. Our services are tailored keeping this in mind.

​

Our services are wide-ranging: from wellness programs and management of HIV to LGBTQ care and psychological counselling. We at RH, are constantly striving to meets the needs of our patients and to ensure the best quality of care.

​
