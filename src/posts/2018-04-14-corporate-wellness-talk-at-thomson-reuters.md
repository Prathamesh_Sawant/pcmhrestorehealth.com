---
title: Corporate Wellness talk at Thomson Reuters
---

PCMH Restore Health team headed to Thomson Reuters to give a talk on various health issues faced by the corporate employees. Importance of adult vaccination, sexual health and mental health are some of the topics covered in this engaging talk. This was followed by a Q & A session where the employees asked some insightful, challenging questions on health care which our team addressed, discussed and shed light upon.

![Image of team at Thomson Reuters](../images/corporate-wellness-talk-at-thomson-reuters-1.webp)
![Image of team at Thomson Reuters](../images/corporate-wellness-talk-at-thomson-reuters-2.webp)
