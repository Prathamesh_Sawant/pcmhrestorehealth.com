---
title: Chikkaballapur 'Mathru Devo Bhava' Camp
---

Every month doctors from PCMH Restore Health Center volunteer at the 'Mathru Devo Bhava' Camp organised by Satya Sai Baba Foundation conducted at Primary Health Centers in Chikkaballapur, spread across the entire district. Doctors from various parts of Bengaluru City volunteer at this Camp to provide ante natal check ups to pregnant women, counselling and identifying high risk cases to take the needful action.

![Team at Chikkaballapur Mathru Devo Bhava Camp](../images/chikkaballapur-mathru-devo-bhava-camp-1.webp)
