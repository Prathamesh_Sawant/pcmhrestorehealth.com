---
title: "Weekly Thursday Clinic at Yeshwanthpur"
---

PCMH Restore Health believes in contributing to the society in whatever measures possible.  Our clinical team visits the Karnataka Network for People Living With HIV/AIDS (KNP+) clinic in Yeshwanthpur to conduct a free clinic for HIV positive individuals.

![](../images/weekly-thursday-clinic-at-yeshwanthpur-1.webp)

KNP+ strives to help those with HIV in multiple spheres of their lives, focusing on systematic and continuous development of people living with HIV/AIDS at local, district and state levels.  Presently, KNP+ has 28 District Level Networks affiliated across Karnataka, and extends continuous support to strengthen them. In these 12 years KNP+ registered a tremendous growth in terms of registration, more than 50,000 people living with HIV/AIDS (PLHA) are coming together under the umbrella of confidence and hope which is the great strength for KNP+. To know more about them visit: http://www.knpplus.in/introduction

![](../images/weekly-thursday-clinic-at-yeshwanthpur-2.webp)

If you wish to refer individuals who are HIV positive with limited affordability, this is the address :  1st Floor, APMC Old Administrative Office, Near RMC Yard Police Station, Tumkur Road, Yashwantapur, Bangalore - 22. Timings : Thursday 3:00 PM to 5:00 PM or contact us at PCMH Restore Health to know more.
