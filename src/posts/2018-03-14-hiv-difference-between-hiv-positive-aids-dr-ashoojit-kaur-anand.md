---
title: "HIV: Difference between HIV positive & AIDS - Dr. Ashoojit Kaur Anand"
---
import Youtube from "../components/youtube-embed"

Listen to Dr. Ashoojit as she answers one of the most frequently asked questions.

<Youtube video="https://www.youtube.com/embed/jO-2NUX5Esg" />
