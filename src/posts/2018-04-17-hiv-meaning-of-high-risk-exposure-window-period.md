---
title: "HIV : Meaning of 'high risk exposure' & window period - Dr. Ramakrishna Prasad"
---
import Youtube from "../components/youtube-embed"

Listen to Dr. RK Prasad explain commonly used HIV terminology.

<Youtube video="https://www.youtube.com/embed/xbudtw_KSgc" />
