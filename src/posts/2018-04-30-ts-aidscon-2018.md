---
title: "TS-AIDSCON 2018"
---

PCMH Restore Health emphasizes the importance of staying abreast with the latest happenings in the medical field. We also believe in contributing to the field and doing our bit to help improve patient care.

Dr. Ashoojit, our clinical director, participated in TS-AIDSCON 2018, the Telangana State AIDS Conference, which focuses on raising global awareness of STDs and AIDS and fighting the stigma surrounding these diseases. The conference was organised on the 15th of April. She presented her take on managing HIV positive patients who were non-adherent to therapy and the factors influencing adherence. This was based on a scientific study carried out by PCMH RH in association with Dr. Milind Bhrushundi - HIV specialist at Lata Mangeshkar Hospital, Nagpur.

![](../images/ts-aidscon-2018-1.webp)
![](../images/ts-aidscon-2018-2.webp)
![](../images/ts-aidscon-2018-3.webp)
![](../images/ts-aidscon-2018-4.webp)

The study strongly corroborated our experience with patients: adherence to treatment is influenced by multiple factors such as stigma associated with the disease, counselling before starting medication, counselling during treatment, social support, financial capabilities etc. As physicians we believe that it is important to take all these factors into consideration while handling patients, particularly those with chronic illnesses.

Our patient centered health care does just that! By putting the patient at the center of our discussion, we take the time to understand not just the patient's but also his/her family’s concerns. We answer their queries and help find solutions that work best for them. This, evidently, leads to the best possible outcome.
