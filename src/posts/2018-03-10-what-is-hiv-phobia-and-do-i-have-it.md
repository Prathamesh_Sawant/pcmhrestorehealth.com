---
title: "What is HIV Phobia and Do I have it?"
category: infectious-diseases
---
import Youtube from "../components/youtube-embed"

Listen to Dr. RK Prasad on HIV phobia and the way to tackle it.

<Youtube video="https://www.youtube.com/embed/QteLXP_gl8s" />