import React from "react"
import PropTypes from "prop-types"
import { useStaticQuery, graphql } from "gatsby"

import Header from "./header"
import SEO from "./seo"
import "./layout.css"

const Layout = ({ children, pageContext }) => {
  const data = useStaticQuery(graphql`
    query BlogPostQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `)
  
  const blogDate = new Date(pageContext.frontmatter.date)
  return (
    <div id="wrapper">
      <SEO title={pageContext.frontmatter.title} description={pageContext.frontmatter.description}/>
      <div class="header-background"></div>
      <Header siteTitle={data.site.siteMetadata.title} />
      <div class="blog-heading">
        <h1>{pageContext.frontmatter.title}</h1>
        Posted on {blogDate.toLocaleDateString('en-UK', {dateStyle: 'long', month: 'long', year: 'numeric', day: 'numeric'})} by admin
      </div>
      <main class="content">{children}</main>
      <footer>

      </footer>
    </div>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
