/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */
const path = require('path')

const collections = {
  '/src/posts': 'blog'
}

// Replacing '/' would result in empty string which is invalid
const replacePath = path => (path === `/` ? path : path.replace(/\/$/, ``))

const extractDetailsFromName = filename => {
  const parts = filename.split('-')
  const date = parts.slice(0, 3).join('-')
  const path = parts.slice(3).join('-')
  return {
    date,
    path
  }
}

const extraInfoFromPath = componentPath => {
  if (componentPath.startsWith(__dirname)) {
    const relativePath = componentPath.slice(__dirname.length)
    const collection = collections[path.dirname(relativePath)]
    if (collection) {
      const { name } = path.parse(relativePath)
      const nameDetails = extractDetailsFromName(name)
      nameDetails.path = path.join(collection, nameDetails.path)
      return {
        collection,
        ...nameDetails 
      }
    }
  }
}

const handleRedirects = (createRedirect, redirect_from, toPath) => {
  if (typeof redirect_from === 'string') {
    redirect_from = [ redirect_from ]
  }
  redirect_from.map(fromPath => createRedirect({ fromPath, toPath, isPermanent: true }))
}

// Implement the Gatsby API “onCreatePage”. This is
// called after every page is created.
exports.onCreatePage = ({ page, actions }) => {
  const { createPage, deletePage, createRedirect } = actions
  const oldPage = Object.assign({}, page)
  
  const extraInfo = extraInfoFromPath(page.component)
  if (extraInfo) {
    page.path = extraInfo.path
    page.context.frontmatter = {
      ...page.context.frontmatter,
      ...extraInfo
    }
    deletePage(oldPage)
    createPage(page)
  }
  if (page.context && page.context.frontmatter && page.context.frontmatter.redirect_from) {
    handleRedirects(createRedirect, page.context.frontmatter.redirect_from, page.path)
  }
}
